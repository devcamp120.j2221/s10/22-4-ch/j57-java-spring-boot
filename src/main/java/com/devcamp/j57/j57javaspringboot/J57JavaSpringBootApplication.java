package com.devcamp.j57.j57javaspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J57JavaSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(J57JavaSpringBootApplication.class, args);
	}

}
