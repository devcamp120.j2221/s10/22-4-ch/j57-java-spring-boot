package com.devcamp.j57.j57javaspringboot.model;

public class Fish extends Animals {
    private int size;
    private boolean canEat;
    
    public void swim() {
        System.out.println("Fish swimming ...");
    }

    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("Fish is not mamal");
    }

    @Override
    public void mate() {
        System.out.println("Fish mating ...");
    }    

    public Fish() {
        
    }
    
    public Fish(int size, boolean canEat) {
        super();
        this.size = size;
        this.canEat = canEat;
    }

    public Fish(int age, String gender, int size, boolean canEat) {
        super(age, gender);
        this.size = size;
        this.canEat = canEat;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isCanEat() {
        return canEat;
    }

    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }
    
    
}
