package com.devcamp.j57.j57javaspringboot.model;

public class Duck extends Animals {
    private String beakColor;

    public void swim() {
        System.out.println("Duck swimming ...");
    }
    public void quack() {
        System.out.println("Duck quacking ...");
    }

    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("Duck is mamal");
    }

    @Override
    public void mate() {
        System.out.println("Duck mating ...");
    }

    public Duck() {
    }

    public Duck(String beakColor) {
        super();
        this.beakColor = beakColor;
    }
    public Duck(int age, String gender, String beakColor) {
        super(age, gender);
        this.beakColor = beakColor;
    }
    public String getBeakColor() {
        return beakColor;
    }
    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }
    
}
