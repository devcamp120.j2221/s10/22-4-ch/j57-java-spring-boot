package com.devcamp.j57.j57javaspringboot.controller;

import java.util.ArrayList;

import com.devcamp.j57.j57javaspringboot.model.Animals;
import com.devcamp.j57.j57javaspringboot.model.Duck;
import com.devcamp.j57.j57javaspringboot.model.Fish;
import com.devcamp.j57.j57javaspringboot.model.Zebra;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnimalController {

    @CrossOrigin
    @GetMapping("/listAnimal")
    public ArrayList<Animals> getListAnimals() {
        ArrayList<Animals> listAnimals = new ArrayList<>();
        Animals duck = new Duck();
        duck.setAge(3);
        duck.setGender("female");
        ((Duck)duck).setBeakColor("red");
        duck.isMammal();
        duck.mate();
        ((Duck)duck).swim();
        ((Duck)duck).quack();

        // Animals fish1 = new Fish();
        // fish1.setAge(10);
        Animals fish = new Fish(10, "male", 30, true);
        Animals Zebra = new Zebra(10, "female", true); 

        listAnimals.add(duck);
        listAnimals.add(fish);
        listAnimals.add(Zebra);

        return listAnimals;
    }    
}
